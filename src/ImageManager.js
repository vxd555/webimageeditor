var imAmount = 5;

function RmImage()
{
	if(imAmount < 1) return;
	imAmount -= 1;
	RefreshImages();
}

function AddImage()
{
	imAmount += 1;
	RefreshImages();
}

function RefreshImages()
{
	var srcs = [];
    $(".showedImage").each(function(){
		srcs.push($(this).attr('src'))
    })
	//var srcs = $('.showedImage').find('img').map(function() { return this.src; }).get();

	var images = "";
	for(var i = 0; i <= imAmount; ++i)
	{
		images += '<div id="im' + i + '" class="divImage">'
				+ '<img src="" alt="" class="showedImage" id="im' + i + 'Image">'
				+ '<input type="file" value="load" onchange="LoadFileAnalize(\'im' + i + '\')" class="inputImage" id="im' + i + 'Input">'
				+ '<label for="im' + i + 'Input">im' + i + '</label>'
				+ '</div>';
	}
	document.getElementById("imagesContainer").innerHTML = images;

	for(var i = 0; i <= imAmount && i < srcs.length; ++i)
	{
		//console.log(srcs[i]);
		document.getElementById("im" + i + "Image").src = srcs[i];
	}
}