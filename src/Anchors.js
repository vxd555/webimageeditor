class Anchor
{
	constructor(posX, posY, type, inout, parent, name, object, subname)
	{
		this.posX	= posX;
		this.posY	= posY;
		this.type	= type;
		this.inout = inout;
		this.parent	= parent;
		this.name	= name;
		this.object	= object;
		this.subname = subname;
		
	}
}

AnchorType =
{
	None			: -1,
    Scalar			: 0,
	Vector			: 1,
	Matrix			: 2
}

AnchorINOUT =
{
	IN				: 0,
	OUT				: 1
}

function CreateAnchor(posX, posY, type, inout, parent, name, anchorls, subname)
{
	switch(type)
	{
		case AnchorType.Scalar:
			var anchorPlane = new THREE.Mesh(geometryAnchor, materialAnchorScalar, subname);
			anchorPlane.position.z = -123;
			anchorPlane.position.x = posX;
			anchorPlane.position.y = posY;

			tempAnchor = new Anchor(posX, posY, type, inout, parent, name, anchorPlane, subname);
			anchorls.push(tempAnchor);
		
			scene.add(anchorPlane);
		break;
		case AnchorType.Vector:
			var anchorPlane = new THREE.Mesh(geometryAnchor, materialAnchorVector, subname);
			anchorPlane.position.z = -123;
			anchorPlane.position.x = posX;
			anchorPlane.position.y = posY;

			tempAnchor = new Anchor(posX, posY, type, inout, parent, name, anchorPlane, subname);
			anchorls.push(tempAnchor);
		
			scene.add(anchorPlane);
		break;
		case AnchorType.Matrix:
			var anchorPlane = new THREE.Mesh(geometryAnchor, materialAnchorMatrix, subname);
			anchorPlane.position.z = -123;
			anchorPlane.position.x = posX;
			anchorPlane.position.y = posY;

			tempAnchor = new Anchor(posX, posY, type, inout, parent, name, anchorPlane, subname);
			anchorls.push(tempAnchor);
		
			scene.add(anchorPlane);
		break;
	}
}