class Node
{
	constructor(width, height, posX, posY, type, on, name)
	{
		this.width	= width;
		this.height	= height;
		this.posX	= posX;
		this.posY	= posY;
		this.type	= type;
		this.on		= on;
		this.name	= name;
	}
}

NodeType =
{
	None			: -1,
    InputImage		: 0,
	OutputImage		: 1,
	Mix				: 2,
	BrightContrast	: 3,
	GreyScale		: 4,
	Sepia			: 5,
	Scale			: 6,
	Flip			: 7,
	Crop			: 8,
	Rotate			: 9,
	TextFild		: 10
}

function LoadStartNodes(list, widgetls, anchorls)
{
	CreateNewNode(list, -280, 20, NodeType.InputImage, widgetls, anchorls);
	CreateNewNode(list, 350, 25, NodeType.OutputImage, widgetls, anchorls);
}

function CreateNodeWindow(node)
{
	var geometry = new THREE.PlaneGeometry(node.width, node.height, 32);
	var texture = new THREE.TextureLoader().load("./gfx/filtrBackground.png");
	texture.repeat.set(1, 1 + 35 / node.height);
	var material = new THREE.MeshBasicMaterial({ map: texture, transparent: true, opacity: 1, color: 0xffffff });
	var nodePlane = new THREE.Mesh(geometry, material);
	nodePlane.position.z = -125;
	nodePlane.position.x = 0;
	nodePlane.position.y = 0;
	objects.push(nodePlane);

	scene.add(nodePlane);
}

function CreateNewNode(list, posX, posY, type, widgetls, anchorls)
{
	var newNode;
	switch(type)
	{
		case NodeType.InputImage:
			newNode = new Node(340, 380, posX, posY, NodeType.InputImage, false, "widget" + list.length);
			CreateWidgedWindow(-90, -30, NodeType.InputImage, list.length, widgetls, anchorls, list);
		break;
		case NodeType.OutputImage:
			newNode = new Node(340, 380, posX, posY, NodeType.OutputImage, false, "widget" + list.length);
			CreateWidgedWindow(-90, -30, NodeType.OutputImage, list.length, widgetls, anchorls, list);
		break;
		case NodeType.Mix:
			newNode = new Node(340, 370, posX, posY, NodeType.Mix, false, "widget" + list.length);
			CreateWidgedWindow(-90, -30, NodeType.Mix, list.length, widgetls, anchorls, list);
		break;
		case NodeType.BrightContrast:
			newNode = new Node(340, 370, posX, posY, NodeType.BrightContrast, false, "widget" + list.length);
			CreateWidgedWindow(-90, -30, NodeType.BrightContrast, list.length, widgetls, anchorls, list);
		break;
		case NodeType.GreyScale:
			newNode = new Node(340, 350, posX, posY, NodeType.GreyScale, false, "widget" + list.length);
			CreateWidgedWindow(-90, -30, NodeType.GreyScale, list.length, widgetls, anchorls, list);
		break;
		case NodeType.Sepia:
			newNode = new Node(340, 350, posX, posY, NodeType.Toning, false, "widget" + list.length);
			CreateWidgedWindow(-90, -30, NodeType.Sepia, list.length, widgetls, anchorls, list);
		break;
		case NodeType.Scale:
			newNode = new Node(340, 370, posX, posY, NodeType.Scale, false, "widget" + list.length);
			CreateWidgedWindow(-90, -30, NodeType.Scale, list.length, widgetls, anchorls, list);
		break;
		case NodeType.Flip:
			newNode = new Node(340, 370, posX, posY, NodeType.Flip, false, "widget" + list.length);
			CreateWidgedWindow(-90, -30, NodeType.Flip, list.length, widgetls, anchorls, list);
		break;
		case NodeType.Crop:
			newNode = new Node(340, 460, posX, posY, NodeType.Crop, false, "widget" + list.length);
			CreateWidgedWindow(-90, -30, NodeType.Crop, list.length, widgetls, anchorls, list);
		break;
		case NodeType.Rotate:
			newNode = new Node(340, 350, posX, posY, NodeType.Rotate, false, "widget" + list.length);
			CreateWidgedWindow(-90, -30, NodeType.Rotate, list.length, widgetls, anchorls, list);
		break;
		case NodeType.TextFild:
			newNode = new Node(340, 350, posX, posY, NodeType.TextFild, false, "widget" + list.length);
			CreateWidgedWindow(-90, -30, NodeType.TextFild, list.length, widgetls, anchorls, list);
		break;
	}

	list.push(newNode);
}
