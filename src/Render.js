function render(deltaTime) //pętla renderująca
{
	for(var i = 0; i < connections.length; ++i) //rysowani lini łączących kotwice
	{
		var selectedObject = scene.getObjectByName(connections[i].name);
    	scene.remove(selectedObject);
	}	
	for(var c = 0; c < connectionList.length; ++c)
	{
		for(var a = 0; a < anchorsList.length; ++a) //aktualizowanie pozycji kotwic
		{
			if(connectionList[c].nameIN == anchorsList[a].name && connectionList[c].subnameIN == anchorsList[a].subname)
			{
				for(var a2 = 0; a2 < anchorsList.length; ++a2) //aktualizowanie pozycji kotwic
				{
					if(connectionList[c].nameOUT == anchorsList[a2].name && connectionList[c].subnameOUT == anchorsList[a2].subname)
					{
						var mid = Vector3GetMiddle(anchorsList[a].object.position, anchorsList[a2].object.position, 0, 0, 0);
						var curve = new THREE.QuadraticBezierCurve3(
							anchorsList[a].object.position,
							Vector3GetMiddle(anchorsList[a].object.position, mid, 0, 35, 0),
							mid
						);
						var curve2 = new THREE.QuadraticBezierCurve3(
							mid,
							Vector3GetMiddle(mid, anchorsList[a2].object.position, 0, -35, 0),
							anchorsList[a2].object.position
						);

						var geometry = new THREE.Geometry();
						geometry.vertices = curve.getPoints(50);
						var line = new THREE.Line(geometry, new THREE.LineBasicMaterial({color: 0xeeeeee}));
						line.name = "line0_" + c;
						connections.push(line);
						scene.add(line);
						var geometry2 = new THREE.Geometry();
						geometry2.vertices = curve2.getPoints(50);
						var line2 = new THREE.Line(geometry2, new THREE.LineBasicMaterial({color: 0xeeeeee}));
						line2.name = "line1_" + c;
						connections.push(line2);
						scene.add(line2);
					}
				}
			}
		}
	}
	
	renderer.render(scene, camera);
}