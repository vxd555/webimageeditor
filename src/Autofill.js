var filtersDictionary = ["grayscale",
						"contrastbrightness",
						"sepia",
						"mix",
						"add",
						"sub",
						"lightest",
						"darkest",
						"difference",
						"scale",
						"treshold",
						"invert",
						"edgedetection",
						"crop",
						"flipx",
						"flipy"];

class CommandEditDistance
{
	constructor(filter, distance)
	{
		this.filter = filter;
		this.distance= distance;
	}
}

function GetHint()
{
	var source = document.getElementById("line_numbers"); //pobranie obszaru z tekstem
	source.focus();

	var sourceLines = source.value.split("\n"); //spisanie tekstu z obszaru i podzielenie go na linie
	var line = source.value.substring(0, source.selectionStart);
	var lines = line.split("\n");

	var lineStart = 0; //wyznaczneie początku i końca lini
	var lineEnd = 0;
	for(var i = 0; i < lines.length - 1; ++i)
	{
		lineStart += lines[i].length;
		lineEnd += sourceLines[i].length;
	}
	lineEnd += sourceLines[lines.length - 1].length;

	var command = sourceLines[lines.length - 1].split(" ");//podzielenie obecnej lini na konkretne fragmenty komendy
	var fit = false;
	if(command.length > 0)
	{
		for(var i = 0; i < filtersDictionary.length - 1; ++i)
		{
			if(command[0] == filtersDictionary[i])
			{
				var hitText = "";
				for(var j = 0; j < sourceLines.length; ++j)
				{
					if(j == lines.length - 1) hitText += (FillHint(i) + "\n");
					else hitText += (sourceLines[j] + "\n");
				}
				source.value = hitText;
				fit = true;
				break;
			}
		}
		if(fit == false)
		{
			if(command[0].length < 6)
			{
				for(var i = 0; i < filtersDictionary.length - 1; ++i)
				{
					if(command[0] == filtersDictionary[i].substr(0, command[0].length > filtersDictionary[i].length ? filtersDictionary[i].length : command[0].length))
					{
						var hitText = "";
						for(var j = 0; j < sourceLines.length; ++j)
						{
							if(j == lines.length - 1) hitText += (FillHint(i) + "\n");
							else hitText += (sourceLines[j] + "\n");
						}
						source.value = hitText;
						break;
					}
				}
			}
			else
			{
				var distances = new Array();
				var min = 1000;
				for(var i = 0; i < filtersDictionary.length - 1; ++i)
				{
					var ced = new CommandEditDistance(i, GetEditDistance(command[0], filtersDictionary[i]));
					distances.push(ced);
					if(ced.distance < min)
					{
						min = ced.distance;
					}
				}
				if(distances.length > 0)
				{
					for(var i = 0; i < distances.length - 1; ++i)
					{
						if(distances[i].distance == min)
						{
							var hitText = "";
							for(var j = 0; j < sourceLines.length; ++j)
							{
								if(j == lines.length - 1) hitText += (FillHint(i) + "\n");
								else hitText += (sourceLines[j] + "\n");
							}
							source.value = hitText;
							break;
						}
					}
				}
			}
		}
	}
}

document.addEventListener("keydown", function(event) {
	if(event.which == 112)
	{
		GetHint();
	}

});

function FillHint(filter)
{
	if(filtersDictionary[filter] == "grayscale")
	{
		return filtersDictionary[filter] + " src dest value";
	}
	if(filtersDictionary[filter] == "contrastbrightness") 
	{
		return filtersDictionary[filter] + " src dest number number";
	}
	if(filtersDictionary[filter] == "sepia") 
	{
		return filtersDictionary[filter] + " src dest";
	}
	if(filtersDictionary[filter] == "mix") 
	{
		return filtersDictionary[filter] + " src src2 dest value";
	}
	if(filtersDictionary[filter] == "add") 
	{
		return filtersDictionary[filter] + " src src2 dest value";
	}
	if(filtersDictionary[filter] == "sub") 
	{
		return filtersDictionary[filter] + " src src2 dest value";
	}
	if(filtersDictionary[filter] == "lightest")
	{
		return filtersDictionary[filter] + " src src2 dest value";
	}
	if(filtersDictionary[filter] == "darkest")
	{
		return filtersDictionary[filter] + " src src2 dest value";
	}
	if(filtersDictionary[filter] == "difference")
	{
		return filtersDictionary[filter] + " src src2 dest";
	}
	if(filtersDictionary[filter] == "scale")
	{
		return filtersDictionary[filter] + " src dest width height";
	}
	if(filtersDictionary[filter] == "treshold")
	{
		return filtersDictionary[filter] + " src dest number";
	}
	if(filtersDictionary[filter] == "invert")
	{
		return filtersDictionary[filter] + " src dest";
	}
	if(filtersDictionary[filter] == "edgedetection")
	{
		return filtersDictionary[filter] + " src dest number";
	}
	if(filtersDictionary[filter] == "crop")
	{
		return filtersDictionary[filter] + " src dest beginX beginY endX endX";
	}
	if(filtersDictionary[filter] == "flipx")
	{ 
		return filtersDictionary[filter] + " src dest";
	}
	if(filtersDictionary[filter] == "flipy")
	{ 
		return filtersDictionary[filter] + " src dest";
	}
	return "";
}

function GetEditDistance(a, b)
{
	if(a.length == 0) return b.length;
	if(b.length == 0) return a.length; 
  
	var matrix = [];
	var i;
	for(i = 0; i <= b.length; i++)
	{
		matrix[i] = [i];
	}

	var j;
	for(j = 0; j <= a.length; j++)
	{
		matrix[0][j] = j;
	}
  
	// Fill in the rest of the matrix
	for(i = 1; i <= b.length; i++)
	{
		for(j = 1; j <= a.length; j++)
		{
			if(b.charAt(i-1) == a.charAt(j-1))
			{
				matrix[i][j] = matrix[i-1][j-1];
			}
			else
			{
		  		matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
								Math.min(matrix[i][j-1] + 1, // insertion
								matrix[i-1][j] + 1)); // deletion
			}
		}
	}
  
	return matrix[b.length][a.length];
  };