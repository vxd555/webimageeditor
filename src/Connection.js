class Connection
{
	constructor(nameOUT, nameIN, subnameOUT, subnameIN)
	{
		this.nameOUT	= nameOUT;
		this.nameIN		= nameIN;
		this.subnameIN	= subnameIN;
		this.subnameOUT	= subnameOUT;
	}
}

function CreateConnection(nameOUT, nameIN, subnameOUT, subnameIN, connectLs, list)
{
	var tempConnection = new Connection(nameOUT, nameIN, subnameOUT, subnameIN);
	connectLs.push(tempConnection);

	UpdateImages(connectLs, list);
}

function UpdateImages(connectLs, list)
{
	var currNode = 0;
	var checkedNode = 0;
	var c = 0;
	for(currNode = 0; currNode < Object.keys(list).length; ++currNode) //znajdowanie wszystkich początków
	{
		if(list[currNode].type == NodeType.InputImage)
		{
			for(c = 0; c < Object.keys(connectLs).length; ++c) //wyszukiwanie przyległych podłączeń
			{
				if(connectLs[c].nameOUT == list[currNode].name)
				{
					for(checkedNode = 0; checkedNode < Object.keys(list).length; ++checkedNode)
					{
						if(connectLs[c].nameIN == list[checkedNode].name && connectLs[c].nameOUT == list[currNode].name)
						{
							document.getElementById(list[checkedNode].name + 'Image').src = document.getElementById(list[currNode].name + 'Image').src;
							//tutaj powinno być uaktywanianie efektów

							currNode = checkedNode;
							c = 0;
						}
					}
				}
			}
			
		}
	}

	
}