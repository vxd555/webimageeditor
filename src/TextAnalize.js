function AnalizeProgram(name)
{
	//var imageObject = document.getElementsByClassName("showedImage");
	document.getElementById("errorlog").innerHTML = "";

	var source = $('#line_numbers').val().split("\n");
	for(var i = 0; i < source.length; ++i)
	{
		var command = source[i].split(" ");
		if(command.length <= 0) continue;

		if(command[0].toLowerCase() == "grayscale")
		{
			if(command.length != 4)
			{
				console.log("command in line " + (i + 1) + " Grayscale" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Grayscale" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Grayscale" + " invalid image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Grayscale" + " invalid image size";
			}
			else if(!(command[3] >= 0.0 && command[3] <= 1.0))
			{
				console.log("command in line " + (i + 1) + " Grayscale" + " invalid parametr 3. value should be in range [0;1]");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Grayscale" + " invalid parametr 3. value should be in range [0;1]";
			}
			else document.getElementById(command[2] + "Image").src = ImageToGrayscale(document.getElementById(command[1] + 'Image'), command[3]);
			//else imgArray[parseInt(command[2].substring(2, command[2].length))] = ImageToGrayscale(imgArray[parseInt(command[1].substring(2, command[1].length))], command[3]);
		}
		else if(command[0].toLowerCase() == "contrastbrightness")
		{
			if(command.length != 5)
			{
				console.log("command in line " + (i + 1) + " Contrast and Brightness" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Contrast and Brightness" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Contrast and Brightness" + " invalid image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Contrast and Brightness" + " invalid image size";
			}
			else if(!(command[3] >= 0 && command[3] <= 255))
			{
				console.log("command in line " + (i + 1) + " Contrast and Brightness" + " invalid parametr 3. value should be in range [0;255]")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Contrast and Brightness" + " invalid parametr 3. value should be in range [0;255]";
			}
			else if(!(command[4] >= 0 && command[4] <= 255))
			{
				console.log("command in line " + (i + 1) + " Contrast and Brightness" + " invalid parametr 4. value should be in range [0;255]")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Contrast and Brightness" + " invalid parametr 4. value should be in range [0;255]";
			}
			else document.getElementById(command[2] + "Image").src = ContrastBrightness(document.getElementById(command[1] + 'Image'), command[3], command[4]);
		}
		else if(command[0].toLowerCase() == "sepia")
		{
			if(command.length != 3)
			{
				console.log("command in line " + (i + 1) + " Sepia" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Sepia" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Sepia" + " invalid image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Sepia" + " invalid image size";
			}
			else document.getElementById(command[2] + "Image").src = Sepia(document.getElementById(command[1] + 'Image'));
		}
		else if(command[0].toLowerCase() == "mix")
		{
			if(command.length != 5)
			{
				console.log("command in line " + (i + 1) + " Mix" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Mix" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Mix" + " invalid first image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Mix" + " invalid first image size";
			}
			else if(document.getElementById(command[2] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Mix" + " invalid second image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Mix" + " invalid second image size";
			}
			else if(!(command[4] >= 0.0 && command[4] <= 1.0))
			{
				console.log("command in line " + (i + 1) + " Mix" + " invalid parametr 3. value should be in range [0;1]")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Mix" + " invalid parametr 3. value should be in range [0;1]";
			}
			else document.getElementById(command[3] + "Image").src = MixBlend(document.getElementById(command[1] + 'Image'), document.getElementById(command[2] + 'Image'), command[4]);
		}
		else if(command[0].toLowerCase() == "add")
		{
			if(command.length != 5)
			{
				console.log("command in line " + (i + 1) + " Add" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Add" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Add" + " invalid first image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Add" + " invalid first image size";
			}
			else if(document.getElementById(command[2] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Add" + " invalid second image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Add" + " invalid second image size";
			}
			else if(!(command[4] >= 0.0 && command[4] <= 1.0))
			{
				console.log("command in line " + (i + 1) + " Add" + " invalid parametr 3. value should be in range [0;1]")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Add" + " invalid parametr 3. value should be in range [0;1]";
			}
			else document.getElementById(command[3] + "Image").src = MixAdd(document.getElementById(command[1] + 'Image'), document.getElementById(command[2] + 'Image'), command[4]);
		}
		else if(command[0].toLowerCase() == "sub")
		{
			if(command.length != 5)
			{
				console.log("command in line " + (i + 1) + " Sub" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Sub" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Sub" + " invalid first image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Sub" + " invalid first image size";
			}
			else if(document.getElementById(command[2] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Sub" + " invalid second image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Sub" + " invalid second image size";
			}
			else if(!(command[4] >= 0.0 && command[4] <= 1.0))
			{
				console.log("command in line " + (i + 1) + " Sub" + " invalid parametr 3. value should be in range [0;1]")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Sub" + " invalid parametr 3. value should be in range [0;1]";
			}
			else document.getElementById(command[3] + "Image").src = MixSub(document.getElementById(command[1] + 'Image'), document.getElementById(command[2] + 'Image'), command[4]);
		}
		else if(command[0].toLowerCase() == "lightest")
		{
			if(command.length != 5)
			{
				console.log("command in line " + (i + 1) + " Lightest" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Lightest" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Lightest" + " invalid first image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Lightest" + " invalid first image size";
			}
			else if(document.getElementById(command[2] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Lightest" + " invalid second image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Lightest" + " invalid second image size";
			}
			else if(!(command[4] >= 0.0 && command[4] <= 1.0))
			{
				console.log("command in line " + (i + 1) + " Lightest" + " invalid parametr 3. value should be in range [0;1]")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Lightest" + " invalid parametr 3. value should be in range [0;1]";
			}
			else document.getElementById(command[3] + "Image").src = MixLightest(document.getElementById(command[1] + 'Image'), document.getElementById(command[2] + 'Image'), command[4]);
		}
		else if(command[0].toLowerCase() == "darkest")
		{
			if(command.length != 5)
			{
				console.log("command in line " + (i + 1) + " Darkest" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Darkest" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Darkest" + " invalid first image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Darkest" + " invalid first image size";
			}
			else if(document.getElementById(command[2] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Darkest" + " invalid second image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Darkest" + " invalid second image size";
			}
			else if(!(command[4] >= 0.0 && command[4] <= 1.0))
			{
				console.log("command in line " + (i + 1) + " Darkest" + " invalid parametr 3. value should be in range [0;1]")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Darkest" + " invalid parametr 3. value should be in range [0;1]";
			}
			else document.getElementById(command[3] + "Image").src = MixDarkest(document.getElementById(command[1] + 'Image'), document.getElementById(command[2] + 'Image'), command[4]);
		}
		else if(command[0].toLowerCase() == "difference")
		{
			if(command.length != 4)
			{
				console.log("command in line " + (i + 1) + " Difference" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Difference" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Difference" + " invalid first image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Difference" + " invalid first image size";
			}
			else if(document.getElementById(command[2] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Difference" + " invalid second image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Difference" + " invalid second image size";
			}
			else document.getElementById(command[3] + "Image").src = MixDifference(document.getElementById(command[1] + 'Image'), document.getElementById(command[2] + 'Image'));
		}
		else if(command[0].toLowerCase() == "scale")
		{
			if(command.length != 5)
			{
				console.log("command in line " + (i + 1) + " Scale" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Scale" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Scale" + " invalid image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Scale" + " invalid image size";
			}
			else if(command[3] <= 0)
			{
				console.log("command in line " + (i + 1) + " Scale" + " invalid parametr 3. value should be greater than 0")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Scale" + " invalid parametr 3. value should be greater than 0";
			}
			else if(command[4] <= 0)
			{
				console.log("command in line " + (i + 1) + " Scale" + " invalid parametr 4. value should be greater than 0")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Scale" + " invalid parametr 4. value should be greater than 0";
			}
			else document.getElementById(command[2] + "Image").src = Scale(document.getElementById(command[1] + 'Image'), command[3], command[4]);
		}
		else if(command[0].toLowerCase() == "treshold")
		{
			if(command.length != 4)
			{
				console.log("command in line " + (i + 1) + " Treshold" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Treshold" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Treshold" + " invalid image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Treshold" + " invalid image size";
			}
			else if(command[3] <= 0)
			{
				console.log("command in line " + (i + 1) + " Treshold" + " invalid parametr 3. number should be in range [0;255]")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Treshold" + " invalid parametr 3. number should be in range [0;255]";
			}
			else document.getElementById(command[2] + "Image").src = Treshold(document.getElementById(command[1] + 'Image'), command[3]);
		}
		else if(command[0].toLowerCase() == "invert")
		{
			if(command.length != 3)
			{
				console.log("command in line " + (i + 1) + " Invert" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Invert" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Invert" + " invalid image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Invert" + " invalid image size";
			}
			else document.getElementById(command[2] + "Image").src = Invert(document.getElementById(command[1] + 'Image'));
			/*
			console.log("in");
			console.log(imageObject[parseInt(command[1].substring(2, command[1].length))].src);
			if(command.length != 3) console.log("command in line " + (i + 1) + " Invert" + " have bad number of parameters");
			else if(imageObject[parseInt(command[1].substring(2, command[1].length))].naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Invert" + " invalid image size");
			}
			else
			{
				imgArray[parseInt(command[2].substring(2, command[2].length))].src = Invert(imgArray[parseInt(command[1].substring(2, command[1].length))]);
				imgArray[parseInt(command[2].substring(2, command[2].length))].
			}
			else
			{
				console.log(imageObject[parseInt(command[2].substring(2, command[2].length))].naturalWidth);
				console.log(imageObject[parseInt(command[2].substring(2, command[2].length))].naturalHeight);
				imageObject[parseInt(command[2].substring(2, command[2].length))].src = Invert(imageObject[parseInt(command[1].substring(2, command[1].length))]);
				//imageObject[parseInt(command[2].substring(2, command[2].length))].naturalWidth = imageObject[parseInt(command[2].substring(2, command[2].length))].naturalWidth;
				//imageObject[parseInt(command[2].substring(2, command[2].length))].naturalHeight = imageObject[parseInt(command[2].substring(2, command[2].length))].naturalHeight;
				console.log(imageObject[parseInt(command[1].substring(2, command[1].length))].naturalWidth);
				console.log(imageObject[parseInt(command[1].substring(2, command[1].length))].naturalHeight);
				console.log(imageObject[parseInt(command[2].substring(2, command[2].length))].naturalWidth);
				console.log(imageObject[parseInt(command[2].substring(2, command[2].length))].naturalHeight);
				console.log(imageObject[parseInt(command[2].substring(2, command[2].length))].width);
				console.log(imageObject[parseInt(command[2].substring(2, command[2].length))].height);
			}
			console.log("out");
			console.log(imageObject[parseInt(command[2].substring(2, command[2].length))].src);*/
		}
		else if(command[0].toLowerCase() == "edgedetection")
		{
			if(command.length != 4)
			{
				console.log("command in line " + (i + 1) + " Edge Detection" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Edge Detection" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Edge Detection" + " invalid image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Edge Detection" + " invalid image size";
			}
			else if(!(command[3] >= 0 && command[3] <= 255))
			{
				console.log("command in line " + (i + 1) + " Edge Detection" + " invalid parametr 3. value should be in range [0;255]")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Edge Detection" + " invalid parametr 3. value should be in range [0;255]";
			}
			else document.getElementById(command[2] + "Image").src = EdgeDetection(document.getElementById(command[1] + 'Image'), command[3]);
		}
		else if(command[0].toLowerCase() == "crop")
		{
			if(command.length != 7)
			{
				console.log("command in line " + (i + 1) + " Crop" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Crop" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Crop" + " invalid image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Crop" + " invalid image size";
			}
			else if(!(command[3] > 0 && command[3] < document.getElementById(command[1] + 'Image').naturalWidth - 1 - command[5]))
			{
				console.log("command in line " + (i + 1) + " Crop" + " invalid parametr 3. value should be greater than or equal 0 and less than image size - 1")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Crop" + " invalid parametr 3. value should be greater than or equal 0 and less than image size - 1";
			}
			else if(!(command[4] > 0 && command[4] < document.getElementById(command[1] + 'Image').naturalHeight - 1 - command[6]))
			{
				console.log("command in line " + (i + 1) + " Crop" + " invalid parametr 4. value should be greater than or equal 0 and less than image size - 1")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Crop" + " invalid parametr 4. value should be greater than or equal 0 and less than image size - 1";
			}
			else if(!(command[5] > 1 && command[5] < document.getElementById(command[1] + 'Image').naturalWidth - command[3]))
			{
				console.log("command in line " + (i + 1) + " Crop" + " invalid parametr 5. value should be greater than 0 and less than image size")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Crop" + " invalid parametr 5. value should be greater than 0 and less than image size";
			}
			else if(!(command[6] > 1 && command[6] < document.getElementById(command[1] + 'Image').naturalHeight - command[4]))
			{
				console.log("command in line " + (i + 1) + " Crop" + " invalid parametr 6. value should be greater than 0 and less than image size")
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Crop" + " invalid parametr 6. value should be greater than 0 and less than image size";
			}
			else document.getElementById(command[2] + "Image").src = Crop(document.getElementById(command[1] + 'Image'), command[3], command[4], command[5], command[6]);
		}
		else if(command[0].toLowerCase() == "flipx")
		{
			if(command.length != 3)
			{
				console.log("command in line " + (i + 1) + " Flip in X axis" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Flip in X axis" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Flip in X axis" + " invalid image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Flip in X axis" + " invalid image size";
			}
			else document.getElementById(command[2] + "Image").src = FlipX(document.getElementById(command[1] + 'Image'));
		}
		else if(command[0].toLowerCase() == "flipy")
		{
			if(command.length != 3)
			{
				console.log("command in line " + (i + 1) + " Flip in Y axis" + " have bad number of parameters");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Flip in Y axis" + " have bad number of parameters";
			}
			else if(document.getElementById(command[1] + 'Image').naturalWidth <= 0)
			{
				console.log("command in line " + (i + 1) + " Flip in Y axis" + " invalid image size");
				document.getElementById("errorlog").innerHTML = "command in line " + (i + 1) + " Flip in Y axis" + " invalid image size";
			}
			else document.getElementById(command[2] + "Image").src = FlipY(document.getElementById(command[1] + 'Image'));
		}
		else if(command[0] == "")
		{
			continue;
		}
		else
		{
			console.log("unknown command in line " + (i + 1) + " " + command[0].toLowerCase());
			document.getElementById("errorlog").innerHTML = "unknown command in line " + (i + 1) + " " + command[0].toLowerCase();
		}
	}

	/*
	for(var i = 0; i < imageObject.length; ++i)
	{
		if(imageObject[i].src != null && document.getElementById(command[i] + "Image") != null)
		{
			document.getElementById("im" + i + "Image").src = imageObject[i].src;
		}
	}
	*/
	
}

function LoadFileAnalize(input)
{
	var preview = document.getElementById(input + "Image");
	var file    = document.getElementById(input + "Input").files[0];
	var reader  = new FileReader();

	reader.addEventListener("load", function ()
	{
		preview.src = reader.result;
	}, false);

	if(file)
	{
		reader.readAsDataURL(file);
	}
}