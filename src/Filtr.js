function Truncate(value)
{
    if(value < 0) return 0;
    if(value > 255) return 255;
    return value;
}

function ImageToGrayscale(imgObj, value)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
     
	for(var y = 0; y < imgPixels.height; ++y)
	{
		for(var x = 0; x < imgPixels.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
            if(value > 0.5) avg += (255 - avg) * (value - 0.5) * 2;
            else if(value < 0.5) avg -= (255 - (255 - avg)) * (1 - (value * 2))
            imgPixels.data[i] = avg; 
            imgPixels.data[i + 1] = avg; 
            imgPixels.data[i + 2] = avg;
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}
function ChangeContrast(imgObj, value)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
     
	for(var y = 0; y < imgPixels.height; ++y)
	{
		for(var x = 0; x < imgPixels.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            var factor = (259 * (value + 255)) / (255 * (259 - value)) 
            imgPixels.data[i] = Truncate(factor * (imgPixels.data[i] - 128) + 128);
            imgPixels.data[i + 1] = Truncate(factor * (imgPixels.data[i + 1] - 128) + 128); 
            imgPixels.data[i + 2] = Truncate(factor * (imgPixels.data[i + 2] - 128) + 128);
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}

function ContrastBrightness(imgObj, contrast, brightness)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
     
	for(var y = 0; y < imgPixels.height; ++y)
	{
		for(var x = 0; x < imgPixels.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            var factor = (259 * (contrast + 255)) / (255 * (259 - contrast)) 
            imgPixels.data[i] = Truncate(factor * (imgPixels.data[i] - 128) + 128 + brightness);
            imgPixels.data[i + 1] = Truncate(factor * (imgPixels.data[i + 1] - 128) + 128 + brightness); 
            imgPixels.data[i + 2] = Truncate(factor * (imgPixels.data[i + 2] - 128) + 128 + brightness);
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}

function Sepia(imgObj)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
     
	for(var y = 0; y < imgPixels.height; ++y)
	{
		for(var x = 0; x < imgPixels.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            var r = 0.393 * imgPixels.data[i] + 0.769 * imgPixels.data[i + 1] + 0.189 * imgPixels.data[i + 2];
            var g = 0.349 * imgPixels.data[i] + 0.686 * imgPixels.data[i + 1] + 0.168 * imgPixels.data[i + 2];
            var b = 0.272 * imgPixels.data[i] + 0.534 * imgPixels.data[i + 1] + 0.131 * imgPixels.data[i + 2];
            imgPixels.data[i] = Truncate(r);
            imgPixels.data[i + 1] = Truncate(g); 
            imgPixels.data[i + 2] = Truncate(b);
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}

function MixBlend(imgObj, imgObj2, value)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;

    var imgW2 = imgObj2.naturalWidth;
    var imgH2 = imgObj2.naturalHeight;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
    canvasContext.drawImage(imgObj2, 0, 0);
    var imgPixels2 = canvasContext.getImageData(0, 0, imgW2, imgH2);
     
	for(var y = 0; y < imgPixels.height && y < imgPixels2.height; ++y)
	{
		for(var x = 0; x < imgPixels.width && x < imgPixels2.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            var j = (y * 4) * imgPixels2.width + x * 4;
            imgPixels.data[i] = Truncate(imgPixels.data[i] * value + imgPixels2.data[j] * (1 - value));
            imgPixels.data[i + 1] = Truncate(imgPixels.data[i + 1] * value + imgPixels2.data[j + 1] * (1 - value)); 
            imgPixels.data[i + 2] = Truncate(imgPixels.data[i + 2] * value + imgPixels2.data[j + 2] * (1 - value));
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
    
    return canvas.toDataURL();
}
function MixAdd(imgObj, imgObj2, value)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;

    var imgW2 = imgObj2.naturalWidth;
    var imgH2 = imgObj2.naturalHeight;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
    canvasContext.drawImage(imgObj2, 0, 0);
    var imgPixels2 = canvasContext.getImageData(0, 0, imgW2, imgH2);
     
	for(var y = 0; y < imgPixels.height && y < imgPixels2.height; ++y)
	{
		for(var x = 0; x < imgPixels.width && x < imgPixels2.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            var j = (y * 4) * imgPixels2.width + x * 4;
            imgPixels.data[i] = Math.min(imgPixels.data[i] * value + imgPixels2.data[j], 255);
            imgPixels.data[i + 1] = Math.min(imgPixels.data[i + 1] * value + imgPixels2.data[j + 1], 255); 
            imgPixels.data[i + 2] = Math.min(imgPixels.data[i + 2] * value + imgPixels2.data[j + 2], 255);
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}
function MixSub(imgObj, imgObj2, value)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;

    var imgW2 = imgObj2.naturalWidth;
    var imgH2 = imgObj2.naturalHeight;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
    canvasContext.drawImage(imgObj2, 0, 0);
    var imgPixels2 = canvasContext.getImageData(0, 0, imgW2, imgH2);
     
	for(var y = 0; y < imgPixels.height && y < imgPixels2.height; ++y)
	{
		for(var x = 0; x < imgPixels.width && x < imgPixels2.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            var j = (y * 4) * imgPixels2.width + x * 4;
            imgPixels.data[i] = Math.max(imgPixels2.data[j] - imgPixels.data[i] * value, 0);
            imgPixels.data[i + 1] = Math.max(imgPixels2.data[j + 1] - imgPixels.data[i + 1] * value, 0); 
            imgPixels.data[i + 2] = Math.max(imgPixels2.data[j + 2] - imgPixels.data[i + 2] * value, 0);
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}

function MixLightest(imgObj, imgObj2, value)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;

    var imgW2 = imgObj2.naturalWidth;
    var imgH2 = imgObj2.naturalHeight;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
    canvasContext.drawImage(imgObj2, 0, 0);
    var imgPixels2 = canvasContext.getImageData(0, 0, imgW2, imgH2);
     
	for(var y = 0; y < imgPixels.height && y < imgPixels2.height; ++y)
	{
		for(var x = 0; x < imgPixels.width && x < imgPixels2.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            var j = (y * 4) * imgPixels2.width + x * 4;
            imgPixels.data[i] = Math.max(imgPixels.data[i] * value, imgPixels2.data[j]);
            imgPixels.data[i + 1] = Math.max(imgPixels.data[i + 1] * value, imgPixels2.data[j + 1]); 
            imgPixels.data[i + 2] = Math.max(imgPixels.data[i + 2] * value, imgPixels2.data[j + 2]);
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}

function MixDarkest(imgObj, imgObj2, value)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;

    var imgW2 = imgObj2.naturalWidth;
    var imgH2 = imgObj2.naturalHeight;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
    canvasContext.drawImage(imgObj2, 0, 0);
    var imgPixels2 = canvasContext.getImageData(0, 0, imgW2, imgH2);
     
	for(var y = 0; y < imgPixels.height && y < imgPixels2.height; ++y)
	{
		for(var x = 0; x < imgPixels.width && x < imgPixels2.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            var j = (y * 4) * imgPixels2.width + x * 4;
            imgPixels.data[i] = Math.min(imgPixels.data[i] * value, imgPixels2.data[j]);
            imgPixels.data[i + 1] = Math.min(imgPixels.data[i + 1] * value, imgPixels2.data[j + 1]); 
            imgPixels.data[i + 2] = Math.min(imgPixels.data[i + 2] * value, imgPixels2.data[j + 2]);
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}

function MixDifference(imgObj, imgObj2)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;

    var imgW2 = imgObj2.naturalWidth;
    var imgH2 = imgObj2.naturalHeight;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
    canvasContext.drawImage(imgObj2, 0, 0);
    var imgPixels2 = canvasContext.getImageData(0, 0, imgW2, imgH2);
     
	for(var y = 0; y < imgPixels.height && y < imgPixels2.height; ++y)
	{
		for(var x = 0; x < imgPixels.width && x < imgPixels2.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            var j = (y * 4) * imgPixels2.width + x * 4;
            imgPixels.data[i] = Truncate(Math.abs(imgPixels.data[i] - imgPixels2.data[j]));
            imgPixels.data[i + 1] = Truncate(Math.abs(imgPixels.data[i + 1] - imgPixels2.data[j + 1])); 
            imgPixels.data[i + 2] = Truncate(Math.abs(imgPixels.data[i + 2] - imgPixels2.data[j + 2]));
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}

function Scale(imgObj, width, height)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = width;
    canvas.height = height;
     
    canvasContext.drawImage(imgObj, 0, 0, width, height);
    var imgPixels = canvasContext.getImageData(0, 0, width, height);
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, width, height);//imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}
function Treshold(imgObj, value) //progowanie, wszystko mniejsze od podanej wartości jest czarne
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
     
	for(var y = 0; y < imgPixels.height; ++y)
	{
		for(var x = 0; x < imgPixels.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
            if(avg < value) avg = 0;
            else avg = 255;
            imgPixels.data[i] = avg; 
            imgPixels.data[i + 1] = avg; 
            imgPixels.data[i + 2] = avg;
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}
function Invert(imgObj) //odwracanie kolorów
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
     
	for(var y = 0; y < imgPixels.height; ++y)
	{
		for(var x = 0; x < imgPixels.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            imgPixels.data[i] = 255 - imgPixels.data[i]; 
            imgPixels.data[i + 1] = 255 - imgPixels.data[i + 1]; 
            imgPixels.data[i + 2] = 255 - imgPixels.data[i + 2];
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}

var canvasContextBlank = undefined;
function EdgeDetection(imgObj, value) //wykrywanie krawędzi
{
    var canvas = document.createElement('canvas');
    var canvasContext = canvas.getContext('2d');
    var canvasBlank = document.createElement('canvas');
    canvasContextBlank = canvasBlank.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;
    canvasBlank.width = imgW;
    canvasBlank.height = imgH;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
    canvasContextBlank.drawImage(imgObj, 0, 0);
    canvasContextBlank.fillStyle = 'black';
    canvasContextBlank.fillRect(0, 0, imgW, imgH);
     
    var left = undefined;
    var top = undefined;
    var right = undefined;
    var bottom = undefined;
    var pixel = undefined;

    
    for(var y = 0; y < imgPixels.height; ++y)
	{
		for(var x = 0; x < imgPixels.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            pixel = imgPixels.data[i + 2];

            left    = imgPixels.data[i - 4];
            right   = imgPixels.data[i + 2];
            top     = imgPixels.data[i - (imgPixels.width * 4)];
            bottom  = imgPixels.data[i + (imgPixels.width * 4)];

            if(pixel > left + value) PlotPoint(x, y);
            else if(pixel < left - value) PlotPoint(x, y);
            else if(pixel > right + value) PlotPoint(x, y);
            else if(pixel < right - value) PlotPoint(x, y);
            else if(pixel > top + value) PlotPoint(x, y);
            else if(pixel < top - value) PlotPoint(x, y);
            else if(pixel > bottom + value) PlotPoint(x, y);
            else if(pixel < bottom - value) PlotPoint(x, y);
        }
    }
    return canvasBlank.toDataURL();
}

function PlotPoint(x, y)
{
    canvasContextBlank.beginPath();
    canvasContextBlank.arc(x, y, 0.5, 0, 1.4 * Math.PI, false);
    canvasContextBlank.fillStyle = 'white';
    canvasContextBlank.fill();
    canvasContextBlank.beginPath();
}

function Crop(imgObj, startX, startY, endX, endY) //przycięcie obrazu
{
    var canvas = document.createElement('canvas');
    var canvasContext = canvas.getContext('2d');
    
    canvas.width = endX;
    canvas.height = endY;
     
    canvasContext.drawImage(imgObj, startX, startY, endX - startX, endY - startY, 0, 0, endX, endY);
	
    return canvas.toDataURL();
}

function FlipX(imgObj)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
    
    for(var y = 0; y < imgPixels.height; ++y)
    {
        for(var x = 0; x < imgPixels.width / 2; ++x)
        {
            var i = (y * 4) * imgPixels.width + x * 4;
            swapR = imgPixels.data[i];
            swapG = imgPixels.data[i + 1];
            swapB = imgPixels.data[i + 2];
            swapA = imgPixels.data[i + 3];

            imgPixels.data[i]       = imgPixels.data[i + (imgPixels.width - x * 2 - 1) * 4]; 
            imgPixels.data[i + 1]   = imgPixels.data[i + (imgPixels.width - x * 2 - 1) * 4 + 1]; 
            imgPixels.data[i + 2]   = imgPixels.data[i + (imgPixels.width - x * 2 - 1) * 4 + 2];
            imgPixels.data[i + 3]   = imgPixels.data[i + (imgPixels.width - x * 2 - 1) * 4 + 3];

            imgPixels.data[i + (imgPixels.width - x * 2 - 1) * 4] = swapR;
            imgPixels.data[i + (imgPixels.width - x * 2 - 1) * 4 + 1] = swapG;
            imgPixels.data[i + (imgPixels.width - x * 2 - 1) * 4 + 2] = swapB;
            imgPixels.data[i + (imgPixels.width - x * 2 - 1) * 4 + 3] = swapA;
        }
    }
    
	
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}

function FlipY(imgObj)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
    

    for(var y = 0; y < imgPixels.height / 2; ++y)
    {
        for(var x = 0; x < imgPixels.width; ++x)
        {
            var i = (y * 4) * imgPixels.width + x * 4;
            swapR = imgPixels.data[i];
            swapG = imgPixels.data[i + 1];
            swapB = imgPixels.data[i + 2];
            swapA = imgPixels.data[i + 3];

            imgPixels.data[i]       = imgPixels.data[(imgPixels.width + x) * 4 + (imgPixels.height - y - 2) * 4 * imgPixels.width]; 
            imgPixels.data[i + 1]   = imgPixels.data[(imgPixels.width + x) * 4 + (imgPixels.height - y - 2) * 4 * imgPixels.width + 1]; 
            imgPixels.data[i + 2]   = imgPixels.data[(imgPixels.width + x) * 4 + (imgPixels.height - y - 2) * 4 * imgPixels.width + 2];
            imgPixels.data[i + 3]   = imgPixels.data[(imgPixels.width + x) * 4 + (imgPixels.height - y - 2) * 4 * imgPixels.width + 3];

            imgPixels.data[(imgPixels.width + x) * 4 + (imgPixels.height - y - 2) * 4 * imgPixels.width] = swapR;
            imgPixels.data[(imgPixels.width + x) * 4 + (imgPixels.height - y - 2) * 4 * imgPixels.width + 1] = swapG;
            imgPixels.data[(imgPixels.width + x) * 4 + (imgPixels.height - y - 2) * 4 * imgPixels.width + 2] = swapB;
            imgPixels.data[(imgPixels.width + x) * 4 + (imgPixels.height - y - 2) * 4 * imgPixels.width + 3] = swapA;
        }
    }
	
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}

function BoxBlur(imgObj, valueX, valueY) //odwracanie kolorów
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
     
	for(var y = 0; y < imgPixels.height; ++y)
	{
		for(var x = 0; x < imgPixels.width; ++x)
		{
            var i = (y * 4) * imgPixels.width + x * 4;
            var avgR = 0;
            var avgG = 0;
            var avgB = 0;
            var avgRamount = 0;
            var avgGamount = 0;
            var avgBamount = 0;
            for(var bY = -valueY / 2; bY < valueY / 2; ++bY)
            {
                for(var bX = -valueX / 2; bX < valueX / 2; ++bX)
                {
                    if(y + bY >= 0 && y < (imgPixels.height - bY)
                    && x + bX >= 0 && x < (imgPixels.width - bX))
                    {
                        avgR += imgPixels.data[i + bX * 4 + bY * imgPixels.width * 4];
                        avgG += imgPixels.data[i + 1 + bX * 4 + bY * imgPixels.width * 4];
                        avgB += imgPixels.data[i + 2 + bX * 4 + bY * imgPixels.width * 4];
                        ++avgRamount;
                        ++avgGamount;
                        ++avgBamount;
                    }
                }
            }
            if(avgRamount != 0)
            {
                imgPixels.data[i] = parseInt(avgR / avgRamount); 
                imgPixels.data[i + 1] = parseInt(avgG / avgGamount); 
                imgPixels.data[i + 2] = parseInt(avgB / avgBamount);
            }
             
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
    return canvas.toDataURL();
}

/*function ChangeBrightness(imgObj, val)
{
    var canvas = document.createElement('canvas');
	var canvasContext = canvas.getContext('2d');
     
    var imgW = imgObj.naturalWidth;
    var imgH = imgObj.naturalHeight;
    canvas.width = imgW;
    canvas.height = imgH;
     
    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
	 
	var dc = new Color(val, val, val, 0);

	for(var y = 0; y < imgPixels.height; y++)
	{
		for(var x = 0; x < imgPixels.width; x++)
		{
			var i = (y * 4) * imgPixels.width + x * 4;
            imgPixels.data[i]
			return src.map(function(c) {
                var nc = c.add(dc);
                return nc.clamp();
            });
        }
    }
	canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
	
	return canvas.toDataURL();
	}
}*/