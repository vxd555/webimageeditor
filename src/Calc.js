function Vector3GetMiddle(start, end, x, y, z)
{
    var middle = new THREE.Vector3();

    middle.x = (start.x + end.x) / 2 + x;
    middle.y = (start.y + end.y) / 2 + y;
    middle.z = (start.z + end.z) / 2 + z;

    return middle;
}