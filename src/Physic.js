function physic(deltaTime) //pętla obliczająca fizykę
{
	for(var i = 0; i < nodeList.length; ++i) //ustawianie pozycji obiektów
	{
		if(nodeList[i].on == false)
		{
			nodeList[i].on = true;
			CreateNodeWindow(nodeList[i]);
		}
		else
		{
			objects[i].position.x = nodeList[i].posX; //aktualizowanie pozycji okna
			objects[i].position.y = nodeList[i].posY;

			for(var a = 0; a < anchorsList.length; ++a) //aktualizowanie pozycji kotwic
			{
				if(nodeList[i].name == anchorsList[a].name)
				{
					anchorsList[a].object.position.x = nodeList[i].posX + anchorsList[a].posX;
					anchorsList[a].object.position.y = nodeList[i].posY + anchorsList[a].posY;
				}
			}

			for(var w = 0; w < widgetList.length; ++w) //aktualizowanie pozycji wnętrza nodów
			{
				if(widgetList[w].parent == catchNodeIndex)
				{
					if(widgetList[w].type == NodeType.InputImage)
					{
						var tempWidget = document.getElementById(widgetList[w].name);
						tempWidget.style.position = "absolute";
						tempWidget.style.left = (mouseGlobalCoordinate.x 
												+ offsetFrameGlobalCoordinate.x 
												- nodeList[i].width / aspect / 2 + 16 / aspect) 
												+ 'px';
						tempWidget.style.top = (mouseGlobalCoordinate.y 
												+ offsetFrameGlobalCoordinate.y 
												- nodeList[i].height / aspect / 2 + 45 / aspect) 
												+ 'px';

						tempWidget = document.getElementById(widgetList[w].name + 'Image');
						tempWidget.style.top = '8px';
						tempWidget = document.getElementById(widgetList[w].name + 'Input');
						tempWidget.style.top = '108px';
						break;
					}
					else if(widgetList[w].type == NodeType.OutputImage)
					{
						var tempWidget = document.getElementById(widgetList[w].name);
						tempWidget.style.position = "absolute";
						tempWidget.style.left = (mouseGlobalCoordinate.x 
												+ offsetFrameGlobalCoordinate.x 
												- nodeList[i].width / aspect / 2 + 16 / aspect) 
												+ 'px';
						tempWidget.style.top = (mouseGlobalCoordinate.y 
												+ offsetFrameGlobalCoordinate.y 
												- nodeList[i].height / aspect / 2 + 50 / aspect) 
												+ 'px';

						tempWidget = document.getElementById(widgetList[w].name + 'Image');
						tempWidget.style.top = '8px';
						tempWidget = document.getElementById(widgetList[w].name + 'Output');
						tempWidget.style.top = '108px';
						break;
					}
					else if(widgetList[w].type == NodeType.Mix)
					{
						var tempWidget = document.getElementById(widgetList[w].name);
						tempWidget.style.position = "absolute";
						tempWidget.style.left = (mouseGlobalCoordinate.x 
												+ offsetFrameGlobalCoordinate.x 
												- nodeList[i].width / aspect / 2 + 16 / aspect) 
												+ 'px';
						tempWidget.style.top = (mouseGlobalCoordinate.y 
												+ offsetFrameGlobalCoordinate.y 
												- nodeList[i].height / aspect / 2 + 50 / aspect) 
												+ 'px';

						tempWidget = document.getElementById(widgetList[w].name + 'Image');
						tempWidget.style.top = '8px';
						tempWidget = document.getElementById(widgetList[w].name + 'Mix');
						tempWidget.style.top = '108px';
						tempWidget = document.getElementById(widgetList[w].name + 'Range');
						tempWidget.style.top = '130px';
						break;
					}
					else if(widgetList[w].type == NodeType.BrightContrast)
					{
						var tempWidget = document.getElementById(widgetList[w].name);
						tempWidget.style.position = "absolute";
						tempWidget.style.left = (mouseGlobalCoordinate.x 
												+ offsetFrameGlobalCoordinate.x 
												- nodeList[i].width / aspect / 2 + 16 / aspect) 
												+ 'px';
						tempWidget.style.top = (mouseGlobalCoordinate.y 
												+ offsetFrameGlobalCoordinate.y 
												- nodeList[i].height / aspect / 2 + 50 / aspect) 
												+ 'px';

						tempWidget = document.getElementById(widgetList[w].name + 'Image');
						tempWidget.style.top = '8px';
						tempWidget = document.getElementById(widgetList[w].name + 'Number');
						tempWidget.style.top = '108px';
						tempWidget = document.getElementById(widgetList[w].name + 'Number2');
						tempWidget.style.top = '130px';
						break;
					}
					else if(widgetList[w].type == NodeType.GreyScale)
					{
						var tempWidget = document.getElementById(widgetList[w].name);
						tempWidget.style.position = "absolute";
						tempWidget.style.left = (mouseGlobalCoordinate.x 
												+ offsetFrameGlobalCoordinate.x 
												- nodeList[i].width / aspect / 2 + 16 / aspect) 
												+ 'px';
						tempWidget.style.top = (mouseGlobalCoordinate.y 
												+ offsetFrameGlobalCoordinate.y 
												- nodeList[i].height / aspect / 2 + 50 / aspect) 
												+ 'px';

						tempWidget = document.getElementById(widgetList[w].name + 'Image');
						tempWidget.style.top = '8px';
						tempWidget = document.getElementById(widgetList[w].name + 'Range');
						tempWidget.style.top = '108px';
						break;
					}
					else if(widgetList[w].type == NodeType.Toning)
					{
						var tempWidget = document.getElementById(widgetList[w].name);
						tempWidget.style.position = "absolute";
						tempWidget.style.left = (mouseGlobalCoordinate.x 
												+ offsetFrameGlobalCoordinate.x 
												- nodeList[i].width / aspect / 2 + 16 / aspect) 
												+ 'px';
						tempWidget.style.top = (mouseGlobalCoordinate.y 
												+ offsetFrameGlobalCoordinate.y 
												- nodeList[i].height / aspect / 2 + 50 / aspect) 
												+ 'px';

						tempWidget = document.getElementById(widgetList[w].name + 'Image');
						tempWidget.style.top = '8px';
						tempWidget = document.getElementById(widgetList[w].name + 'Range');
						tempWidget.style.top = '108px';
						break;
					}
					else if(widgetList[w].type == NodeType.Scale)
					{
						var tempWidget = document.getElementById(widgetList[w].name);
						tempWidget.style.position = "absolute";
						tempWidget.style.left = (mouseGlobalCoordinate.x 
												+ offsetFrameGlobalCoordinate.x 
												- nodeList[i].width / aspect / 2 + 16 / aspect) 
												+ 'px';
						tempWidget.style.top = (mouseGlobalCoordinate.y 
												+ offsetFrameGlobalCoordinate.y 
												- nodeList[i].height / aspect / 2 + 50 / aspect) 
												+ 'px';

						tempWidget = document.getElementById(widgetList[w].name + 'Image');
						tempWidget.style.top = '8px';
						tempWidget = document.getElementById(widgetList[w].name + 'Number');
						tempWidget.style.top = '108px';
						tempWidget = document.getElementById(widgetList[w].name + 'Number2');
						tempWidget.style.top = '130px';
						break;
					}
					else if(widgetList[w].type == NodeType.Flip)
					{
						var tempWidget = document.getElementById(widgetList[w].name);
						tempWidget.style.position = "absolute";
						tempWidget.style.left = (mouseGlobalCoordinate.x 
												+ offsetFrameGlobalCoordinate.x 
												- nodeList[i].width / aspect / 2 + 16 / aspect) 
												+ 'px';
						tempWidget.style.top = (mouseGlobalCoordinate.y 
												+ offsetFrameGlobalCoordinate.y 
												- nodeList[i].height / aspect / 2 + 50 / aspect) 
												+ 'px';

						tempWidget = document.getElementById(widgetList[w].name + 'Image');
						tempWidget.style.top = '8px';
						tempWidget = document.getElementById(widgetList[w].name + 'Checkbox');
						tempWidget.style.top = '108px';
						tempWidget = document.getElementById(widgetList[w].name + 'Checkbox2');
						tempWidget.style.top = '130px';
						break;
					}
					else if(widgetList[w].type == NodeType.Crop)
					{
						var tempWidget = document.getElementById(widgetList[w].name);
						tempWidget.style.position = "absolute";
						tempWidget.style.left = (mouseGlobalCoordinate.x 
												+ offsetFrameGlobalCoordinate.x 
												- nodeList[i].width / aspect / 2 + 16 / aspect) 
												+ 'px';
						tempWidget.style.top = (mouseGlobalCoordinate.y 
												+ offsetFrameGlobalCoordinate.y 
												- nodeList[i].height / aspect / 2 + 50 / aspect) 
												+ 'px';

						tempWidget = document.getElementById(widgetList[w].name + 'Image');
						tempWidget.style.top = '8px';
						tempWidget = document.getElementById(widgetList[w].name + 'Number');
						tempWidget.style.top = '108px';
						tempWidget = document.getElementById(widgetList[w].name + 'Number2');
						tempWidget.style.top = '128px';
						tempWidget = document.getElementById(widgetList[w].name + 'Number3');
						tempWidget.style.top = '148px';
						tempWidget = document.getElementById(widgetList[w].name + 'Number4');
						tempWidget.style.top = '168px';
						break;
					}
					else if(widgetList[w].type == NodeType.Rotate)
					{
						var tempWidget = document.getElementById(widgetList[w].name);
						tempWidget.style.position = "absolute";
						tempWidget.style.left = (mouseGlobalCoordinate.x 
												+ offsetFrameGlobalCoordinate.x 
												- nodeList[i].width / aspect / 2 + 16 / aspect) 
												+ 'px';
						tempWidget.style.top = (mouseGlobalCoordinate.y 
												+ offsetFrameGlobalCoordinate.y 
												- nodeList[i].height / aspect / 2 + 50 / aspect) 
												+ 'px';

						tempWidget = document.getElementById(widgetList[w].name + 'Image');
						tempWidget.style.top = '8px';
						tempWidget = document.getElementById(widgetList[w].name + 'Number');
						tempWidget.style.top = '108px';
						break;
					}
					else if(widgetList[w].type == NodeType.Rotate)
					{
						var tempWidget = document.getElementById(widgetList[w].name);
						tempWidget.style.position = "absolute";
						tempWidget.style.left = (mouseGlobalCoordinate.x 
												+ offsetFrameGlobalCoordinate.x 
												- nodeList[i].width / aspect / 2 + 16 / aspect) 
												+ 'px';
						tempWidget.style.top = (mouseGlobalCoordinate.y 
												+ offsetFrameGlobalCoordinate.y 
												- nodeList[i].height / aspect / 2 + 50 / aspect) 
												+ 'px';

						tempWidget = document.getElementById(widgetList[w].name + 'TextField');
						tempWidget.style.top = '8px';
						break;
					}
				}
			}
		}
	}
}