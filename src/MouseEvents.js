function onDocumentMouseMove(event) //wykrywanie pozycji myszy
{
	mouseX = (event.clientX - windowHalfX) / 2;
	mouseY = (event.clientY - windowHalfY) / 2;
	mouseGlobalCoordinate.x = event.clientX;
	mouseGlobalCoordinate.y = event.clientY;

	if(catchNodeIndex != -1) //pozycjonowanie okna
	{
		nodeList[catchNodeIndex].posX = mouseX * aspect * 2 + offsetFrame.x;
		nodeList[catchNodeIndex].posY = -mouseY * aspect * 2 + offsetFrame.y;
	}
}

function onDocumentMouseDown(event) //kliknięcie myszy
{
	event.preventDefault();
	mouse.x = (event.clientX / renderer.domElement.clientWidth) * 2 - 1;
	mouse.y = -(event.clientY / renderer.domElement.clientHeight) * 2 + 1;
	mouseGlobalCoordinate.x = event.clientX;
	mouseGlobalCoordinate.y = event.clientY;

	raycaster.setFromCamera(mouse, camera);

	var intersects = raycaster.intersectObjects(objects);

	if(intersects.length > 0)
	{
		var tempIndex = objects.indexOf(intersects[0].object);
		
		offsetFrame.x = mouse2NodeOffsetX(nodeList[tempIndex].posX, event.clientX); //obliczanie offsetu okna względem myszy
		offsetFrame.y = mouse2NodeOffseY(nodeList[tempIndex].posY, event.clientY);
		offsetFrameGlobalCoordinate.x = -(event.clientX - node2MouseCoordinateX(nodeList[tempIndex].posX));
		offsetFrameGlobalCoordinate.y = -(event.clientY - node2MouseCoordinateY(nodeList[tempIndex].posY));

		if(offsetFrame.y < -(nodeList[tempIndex].height / 2 - 40 * aspect)) //można chwytać tylko za górną część okna
		{
			catchNodeIndex = objects.indexOf(intersects[0].object); //podpinanie okna
		}
	}

	for(var a = 0; a < anchorsList.length; ++a) //aktualizowanie pozycji kotwic
	{
		if(Length(anchorsList[a].object.position.x, anchorsList[a].object.position.y, mouse2NodeX(event.clientX), -mouse2NodeY(event.clientY)) < 10)
		{
			if(currentAnchor != null) //łączenie
			{
				if(currentAnchor == anchorsList[a].name) //odznaczanie
				{
					currentAnchor = null;
					currentAnchorSubname = null;
					currentAnchorINOUT = null;
				}
				else
				{
					if(currentAnchorINOUT == AnchorINOUT.IN && anchorsList[a].inout == AnchorINOUT.OUT) //łączenie wychodzącym z wchodzącego
					{
						CreateConnection(anchorsList[a].name, currentAnchor, anchorsList[a].subname, currentAnchorSubname, connectionList, nodeList);
						currentAnchor = null;
						currentAnchorSubname = null;
						currentAnchorINOUT = null;
					}
					else if(currentAnchorINOUT == AnchorINOUT.OUT && anchorsList[a].inout == AnchorINOUT.IN) //łączenie wchodzącego z wychodzącym
					{
						CreateConnection(currentAnchor, anchorsList[a].name, currentAnchorSubname, anchorsList[a].subname, connectionList, nodeList);
						currentAnchor = null;
						currentAnchorSubname = null;
						currentAnchorINOUT = null;
					}
					else
					{
						currentAnchor = null;
						currentAnchorSubname = null;
						currentAnchorINOUT = null;
					}
				}
			}
			else //zaznaczanie pierwszego
			{
				currentAnchor = anchorsList[a].name;
				currentAnchorSubname = anchorsList[a].subname;
				currentAnchorINOUT = anchorsList[a].inout;
			}
		}
	}
}
function Length(pX, pY, p2X, p2Y)
{
	return Math.sqrt(Math.pow(pX - p2X, 2) + Math.pow(pY - p2Y, 2));
}

function node2MouseCoordinateX(pX)
{
	return ((pX + window.innerWidth * aspect / 2) / aspect);
}
function node2MouseCoordinateY(pY)
{
	return ((window.innerHeight * aspect - (pY + window.innerHeight * aspect / 2)) / aspect)
}
function mouse2NodeOffsetX(pX, eX)
{
	return pX - (eX- windowHalfX) * aspect
}
function mouse2NodeOffseY(pY, eY)
{
	return pY + (eY - windowHalfY) * aspect
}

function mouse2NodeX(eX)
{
	return (eX- windowHalfX) * aspect
}
function mouse2NodeY(eY)
{
	return (eY - windowHalfY) * aspect
}

function onDocumentMouseUp(event) //puszczenie myszy
{
	catchNodeIndex = -1;
}