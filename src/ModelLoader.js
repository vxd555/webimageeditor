var geometryAnchor;
var materialAnchorScalar;
var materialAnchorVector;
var materialAnchorMatrix;

function LoadBackGround() //załadowanie krateczki w tle
{
	var geometry = new THREE.PlaneGeometry(window.innerWidth * aspect, window.innerHeight * aspect, 32);
	var texture = new THREE.TextureLoader().load("./gfx/background.png");
	texture.wrapS = THREE.RepeatWrapping;
	texture.wrapT = THREE.RepeatWrapping;
	texture.repeat.set(window.innerWidth / (window.innerWidth + window.innerHeight) * 30, 
						window.innerHeight / (window.innerWidth + window.innerHeight) * 30);
	var material = new THREE.MeshBasicMaterial({ map: texture, transparent: true, opacity: 1, color: 0xffffff });
	var backgroundPlane = new THREE.Mesh(geometry, material);
	backgroundPlane.position.z = -127;
	backgroundPlane.position.x = 0;
	backgroundPlane.position.y = 0;

	scene.add(backgroundPlane);
}

function LoadTest()
{
	var geometry = new THREE.PlaneGeometry(window.innerWidth * aspect / 2, window.innerHeight * aspect / 2, 32);
	var texture = new THREE.TextureLoader().load("./gfx/test.png");
	var material = new THREE.MeshBasicMaterial({ map: texture, transparent: true, opacity: 1, color: 0xffffff });
	var backgroundPlane = new THREE.Mesh(geometry, material);
	backgroundPlane.position.z = -127;
	backgroundPlane.position.x = 0;
	backgroundPlane.position.y = 0;

	mainobject = backgroundPlane;
	scene.add(backgroundPlane);
}

function LoadAnchors() //załadowanie geometrii i stworzenie uchwytów do kotwic
{
	geometryAnchor = new THREE.PlaneGeometry(7 * aspect, 6 * aspect, 32);
	var textureAnchor = new THREE.TextureLoader().load("./gfx/PointScalar.png");
	materialAnchorScalar = new THREE.MeshBasicMaterial({ map: textureAnchor, transparent: true, opacity: 1, color: 0xffffff });
	textureAnchor = new THREE.TextureLoader().load("./gfx/PointVector.png");
	materialAnchorVector = new THREE.MeshBasicMaterial({ map: textureAnchor, transparent: true, opacity: 1, color: 0xffffff });
	textureAnchor = new THREE.TextureLoader().load("./gfx/PointMatrix.png");
	materialAnchorMatrix = new THREE.MeshBasicMaterial({ map: textureAnchor, transparent: true, opacity: 1, color: 0xffffff });
}

function ChangeTexture()
{
	
}