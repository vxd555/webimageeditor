class Widget
{
	constructor(posX, posY, type, parent, name)
	{
		this.posX	= posX;
		this.posY	= posY;
		this.type	= type;
		this.parent	= parent;
		this.name	= name;
	}
}

function CreateWidgedWindow(posX, posY, type, parent, widgetls, anchorls, list)
{
	switch(type)
	{
		case NodeType.InputImage:
			var handleWidget = document.createElement('div');
			handleWidget.id = 'widget' + widgetls.length;
			document.body.appendChild(handleWidget);

			var widgetID = handleWidget.id;
			handleWidget.innerHTML = '<div id="'
									+ handleWidget.id
									+ 'Element" width="120">'
									+ '<img src="" width="120" alt="" class="previewImage" id="' 
									+ handleWidget.id
									+ 'Image">'
									+ '<input type="file" value="load" onchange="LoadFile(\''
									+ widgetID
									+ '\')" class="inputImage" id="'
									+ handleWidget.id
									+ 'Input">'
									+ '</div>';

			var newWidget = new Widget(posX, posY, NodeType.InputImage, parent, widgetID);
			widgetls.push(newWidget);

			CreateAnchor(170, 100, AnchorType.Matrix, AnchorINOUT.OUT, parent, widgetID, anchorls, 0);
		break;
		case NodeType.OutputImage:
			var handleWidget = document.createElement('div');
			handleWidget.id = 'widget' + widgetls.length;
			document.body.appendChild(handleWidget);

			var widgetID = handleWidget.id;
			handleWidget.innerHTML = '<div id="'
									+ handleWidget.id
									+ 'Element" width="120">'
									+ '<img src="" width="120" alt="" class="previewImage" id="' 
									+ handleWidget.id
									+ 'Image">'
									+ '<input type="button" value="save" onchange="SaveFile(\''
									+ widgetID
									+ '\')" class="inputImage" id="'
									+ handleWidget.id
									+ 'Output">'
									+ '</div>';

			var newWidget = new Widget(posX, posY, NodeType.OutputImage, parent, widgetID);
			widgetls.push(newWidget);

			CreateAnchor(-170, 100, AnchorType.Matrix, AnchorINOUT.IN, parent, widgetID, anchorls, 0);
		break;
		case NodeType.Mix:
			var handleWidget = document.createElement('div');
			handleWidget.id = 'widget' + widgetls.length;
			document.body.appendChild(handleWidget);

			var widgetID = handleWidget.id;
			handleWidget.innerHTML = '<div id="'
									+ handleWidget.id
									+ 'Element" width="120">'
									+ '<img src="" width="120" alt="" class="previewImage" id="' 
									+ handleWidget.id
									+ 'Image">'
									+ '<select name="mix type" style="width: 100px;" class="inputImage" id="'
									+ handleWidget.id
									+ 'Mix">'
									+ '<option value="blend">blend</option>'
									+ '<option value="add">add</option>'
									+ '<option value="multiply">multiply</option>'
									+ '<option value="subtract">subtract</option>'
									+ '<option value="diffrence">diffrence</option>'
									+ '<option value="lighten">lighten</option>'
									+ '<option value="darken">darken</option>'
									+ '</select>'
									+ '<input type="range" class="inputImage" step="0.001" min="0" max="1" value="0" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Range">'
									+ '</div>';

			var newWidget = new Widget(posX, posY, NodeType.Mix, parent, widgetID);
			widgetls.push(newWidget);

			CreateAnchor(170, 100, AnchorType.Matrix, AnchorINOUT.OUT, parent, widgetID, anchorls, 2);
			CreateAnchor(-170, 50, AnchorType.Matrix, AnchorINOUT.IN, parent, widgetID, anchorls, 0);
			CreateAnchor(-170, 100, AnchorType.Matrix, AnchorINOUT.IN, parent, widgetID, anchorls, 1);
		break;
		case NodeType.BrightContrast:
			var handleWidget = document.createElement('div');
			handleWidget.id = 'widget' + widgetls.length;
			document.body.appendChild(handleWidget);

			var widgetID = handleWidget.id;
			handleWidget.innerHTML = '<div id="'
									+ handleWidget.id
									+ 'Element" width="120">'
									+ '<img src="" width="120" alt="" class="previewImage" id="' 
									+ handleWidget.id
									+ 'Image">'
									+ '<input type="number" class="inputImage" step="0.1" min="0" max="200" value="0" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Number">'
									+ '<input type="number" class="inputImage" step="0.1" min="0" max="200" value="0" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Number2">'
									+ '</div>';

			var newWidget = new Widget(posX, posY, NodeType.BrightContrast, parent, widgetID);
			widgetls.push(newWidget);

			CreateAnchor(170, 100, AnchorType.Matrix, AnchorINOUT.OUT, parent, widgetID, anchorls, 1);
			CreateAnchor(-170, 100, AnchorType.Matrix, AnchorINOUT.IN, parent, widgetID, anchorls, 0);
		break;
		case NodeType.GreyScale:
			var handleWidget = document.createElement('div');
			handleWidget.id = 'widget' + widgetls.length;
			document.body.appendChild(handleWidget);

			var widgetID = handleWidget.id;
			handleWidget.innerHTML = '<div id="'
									+ handleWidget.id
									+ 'Element" width="120">'
									+ '<img src="" width="120" alt="" class="previewImage" id="' 
									+ handleWidget.id
									+ 'Image">'
									+ '<input type="range" class="inputImage" step="0.001" min="0" max="1" value="0" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Range">'
									+ '</div>';

			var newWidget = new Widget(posX, posY, NodeType.GreyScale, parent, widgetID);
			widgetls.push(newWidget);

			CreateAnchor(170, 100, AnchorType.Matrix, AnchorINOUT.OUT, parent, widgetID, anchorls, 1);
			CreateAnchor(-170, 100, AnchorType.Matrix, AnchorINOUT.IN, parent, widgetID, anchorls, 0);
		break;
		case NodeType.Sepia:
			var handleWidget = document.createElement('div');
			handleWidget.id = 'widget' + widgetls.length;
			document.body.appendChild(handleWidget);

			var widgetID = handleWidget.id;
			handleWidget.innerHTML = '<div id="'
									+ handleWidget.id
									+ 'Element" width="120">'
									+ '<img src="" width="120" alt="" class="previewImage" id="' 
									+ handleWidget.id
									+ 'Image">'
									+ '<input type="range" class="inputImage" step="0.001" min="0" max="1" value="0" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Range">'
									+ '</div>';

			var newWidget = new Widget(posX, posY, NodeType.Sepia, parent, widgetID);
			widgetls.push(newWidget);

			CreateAnchor(170, 100, AnchorType.Matrix, AnchorINOUT.OUT, parent, widgetID, anchorls, 1);
			CreateAnchor(-170, 100, AnchorType.Matrix, AnchorINOUT.IN, parent, widgetID, anchorls, 0);
		break;
		case NodeType.Scale:
			var handleWidget = document.createElement('div');
			handleWidget.id = 'widget' + widgetls.length;
			document.body.appendChild(handleWidget);

			var widgetID = handleWidget.id;
			handleWidget.innerHTML = '<div id="'
									+ handleWidget.id
									+ 'Element" width="120">'
									+ '<img src="" width="120" alt="" class="previewImage" id="' 
									+ handleWidget.id
									+ 'Image">'
									+ '<input type="number" class="inputImage" step="0.1" min="0" max="200" value="0" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Number">'
									+ '<input type="number" class="inputImage" step="0.1" min="0" max="200" value="0" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Number2">'
									+ '</div>';

			var newWidget = new Widget(posX, posY, NodeType.Scale, parent, widgetID);
			widgetls.push(newWidget);

			CreateAnchor(170, 100, AnchorType.Matrix, AnchorINOUT.OUT, parent, widgetID, anchorls, 1);
			CreateAnchor(-170, 100, AnchorType.Matrix, AnchorINOUT.IN, parent, widgetID, anchorls, 0);
		break;
		case NodeType.Flip:
			var handleWidget = document.createElement('div');
			handleWidget.id = 'widget' + widgetls.length;
			document.body.appendChild(handleWidget);

			var widgetID = handleWidget.id;
			handleWidget.innerHTML = '<div id="'
									+ handleWidget.id
									+ 'Element" width="120">'
									+ '<img src="" width="120" alt="" class="previewImage" id="' 
									+ handleWidget.id
									+ 'Image">'
									+ '<input type="checkbox" class="inputImage" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Checkbox">'
									+ '<input type="checkbox" class="inputImage" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Checkbox2">'
									+ '</div>';

			var newWidget = new Widget(posX, posY, NodeType.Flip, parent, widgetID);
			widgetls.push(newWidget);

			CreateAnchor(170, 100, AnchorType.Matrix, AnchorINOUT.OUT, parent, widgetID, anchorls, 1);
			CreateAnchor(-170, 100, AnchorType.Matrix, AnchorINOUT.IN, parent, widgetID, anchorls, 0);
		break;
		case NodeType.Crop:
			var handleWidget = document.createElement('div');
			handleWidget.id = 'widget' + widgetls.length;
			document.body.appendChild(handleWidget);

			var widgetID = handleWidget.id;
			handleWidget.innerHTML = '<div id="'
									+ handleWidget.id
									+ 'Element" width="120">'
									+ '<img src="" width="120" alt="" class="previewImage" id="' 
									+ handleWidget.id
									+ 'Image">'
									+ '<input type="number" class="inputImage" step="0.1" min="0" max="200" value="0" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Number">'
									+ '<input type="number" class="inputImage" step="0.1" min="0" max="200" value="0" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Number2">'
									+ '<input type="number" class="inputImage" step="0.1" min="0" max="200" value="0" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Number3">'
									+ '<input type="number" class="inputImage" step="0.1" min="0" max="200" value="0" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Number4">'
									+ '</div>';

			var newWidget = new Widget(posX, posY, NodeType.Crop, parent, widgetID);
			widgetls.push(newWidget);

			CreateAnchor(170, 100, AnchorType.Matrix, AnchorINOUT.OUT, parent, widgetID, anchorls, 1);
			CreateAnchor(-170, 100, AnchorType.Matrix, AnchorINOUT.IN, parent, widgetID, anchorls, 0);
		break;
		case NodeType.Rotate:
			var handleWidget = document.createElement('div');
			handleWidget.id = 'widget' + widgetls.length;
			document.body.appendChild(handleWidget);

			var widgetID = handleWidget.id;
			handleWidget.innerHTML = '<div id="'
									+ handleWidget.id
									+ 'Element" width="120">'
									+ '<img src="" width="120" alt="" class="previewImage" id="' 
									+ handleWidget.id
									+ 'Image">'
									+ '<input type="number" class="inputImage" step="0.1" min="0" max="200" value="0" style="width: 70px;" id="'
									+ handleWidget.id
									+ 'Number">'
									+ '</div>';

			var newWidget = new Widget(posX, posY, NodeType.Rotate, parent, widgetID);
			widgetls.push(newWidget);

			CreateAnchor(170, 100, AnchorType.Matrix, AnchorINOUT.OUT, parent, widgetID, anchorls, 1);
			CreateAnchor(-170, 100, AnchorType.Matrix, AnchorINOUT.IN, parent, widgetID, anchorls, 0);
		break;
		case NodeType.TextFild:
			var handleWidget = document.createElement('div');
			handleWidget.id = 'widget' + widgetls.length;
			document.body.appendChild(handleWidget);

			var widgetID = handleWidget.id;
			handleWidget.innerHTML = '<div id="'
									+ handleWidget.id
									+ 'Element" width="120">'
									+ '<textarea rows="4" cols="50" class="inputImage" id="' 
									+ handleWidget.id
									+ 'TextField"></textarea>'
									+ '</div>';

			var newWidget = new Widget(posX, posY, NodeType.TextFild, parent, widgetID);
			widgetls.push(newWidget);

			CreateAnchor(170, 100, AnchorType.Matrix, AnchorINOUT.OUT, parent, widgetID, anchorls, 1);
			CreateAnchor(-170, 100, AnchorType.Matrix, AnchorINOUT.IN, parent, widgetID, anchorls, 0);
		break;
	}
}

